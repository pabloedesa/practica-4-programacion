package ejercicio1y3;
/**
* Torres Edesa, Pablo
* Practica Evaluable Unidad 6
* Ejercicio 3 totalmente implementado
*/
public class P4Ejercicio3 
{
    public static void main(String[] args) 
    {
		Movil m1 = new Movil ("655 86 32 11", "rata");
		Movil m2 = new Movil ("644 74 64 99", "mono");
		Movil m3 = new Movil ("622 79 08 24", "bisonte");
		System.out.println(m2);
		m1.llama(m2,320);
		m1.llama(m3,200);
		m2.llama(m3,550);
		System.out.println(m1);
		System.out.println(m2);
		System.out.println(m3);
    }
}

class Movil extends Terminal
{
	// Atributos
	private final String TARIFA;
	private float consumo;

	// Métodos
	@Override
	public void llama (Terminal receptor, int duracion)
	{
		super.llama(receptor, duracion);
		switch (TARIFA)
		{
			case "rata":
				consumo += (float)(duracion / 60) * 0.06;
				break;
			case "mono":
				consumo += (float)(duracion / 60) * 0.12;
				break;
			case "bisonte":
				consumo += (float)(duracion / 60) * 0.30;
				break;
		}
	}
	@Override
	public String toString ()
	{
		return super.toString() + " - " + String.format("%.2f", consumo) + " euros tarificados";
	}
	// Constructor
	public Movil (String numeroTelefono, String tarifa)
	{
		super(numeroTelefono);
		this.TARIFA = tarifa.toLowerCase();
	}
}
