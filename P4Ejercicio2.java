/*
 Torres Edesa, Pablo
 Practica Evaluable Unidad 6
 Ejercicio 2 totalmente implementado
*/
import java.util.Scanner;

public class P4Ejercicio2
{
	public static void main (String [] args)
	{
		Scanner sc = new Scanner(System.in);
		Bicicleta bici1 = new Bicicleta ();
		Coche coche1 = new Coche ();
		int opcion;
		do
		{
			System.out.println("1. Monta con la bicicleta \n2. Haz el caballito con la bicicleta \n3. Conduce el coche \n4. Quema rueda con el coche \n5. Ver kilometraje de la bicicleta \n6. Ver kilometraje del coche \n7. Ver kilometraje total (de la bicicleta y el coche) \n8. Salir");
			opcion = sc.nextInt();
			int nuevosKm;
			switch (opcion)
			{
				case 1:
					System.out.print("Cuántos kilómetros quieres recorrer?: ");
					nuevosKm = sc.nextInt();
					bici1.setKilometrosRecorridos(nuevosKm);
					break;
				case 2:
					bici1.hacerCaballito();		
					break;
				case 3:
					System.out.print("Cuántos kilómetros quieres recorrer?: ");
					nuevosKm = sc.nextInt();
					coche1.setKilometrosRecorridos(nuevosKm);
					break;
				case 4:
					coche1.quemarRueda();
					break;
				case 5:
					System.out.println("La bicicleta lleva recorridos: " + bici1 + " Kilómetros");
					break;
				case 6:
					System.out.println("El coche lleva recorridos: " + coche1 + " Kilómetros");
					break;
				case 7:
					System.out.println("Los vehículos llevan recorridos: " + Vehiculo.getKilometrosTotales() + " Kilómetros");
					break;
				default:
					break;
			}
			System.out.println("");
		}
		while (opcion != 8);
		System.out.println("Ha salido de la aplicación");
	}
}	
abstract class Vehiculo
{
	// Atributos
	static private int vehiculosCreados;
	static private int kilometrosTotales;
	private int kilometrosRecorridos;
	// Getters
	public static int getKilometrosTotales ()
	{
		return kilometrosTotales;
	}
	public int getKilometrosRecorridos ()
	{
		return kilometrosRecorridos;
	}
	// Setters
	public void setKilometrosRecorridos (int nuevosKm)
	{
		kilometrosRecorridos += nuevosKm;
		kilometrosTotales += nuevosKm;		
	} 
	
	@Override
	public String toString ()	
	{
		return Integer.toString(getKilometrosRecorridos());
	}
	// Constructor
	protected Vehiculo	()
	{
		vehiculosCreados ++;
		kilometrosRecorridos = 0;
	}
}
class Coche extends Vehiculo 
{
	// Métodos
	public void quemarRueda ()
	{
		System.out.println("Quemando rueda!");
	}
}
class Bicicleta extends Vehiculo 
{
	// Métodos
	public void hacerCaballito ()
	{
		System.out.println("Haciendo el caballito con la bicicleta!");
	}
}

