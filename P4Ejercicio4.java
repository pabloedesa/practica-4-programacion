package ejercicio4;

public class P4Ejercicio4 {
    public static class Publicacion {
        // Atributos
        private String isbn;
        private int anyo;
        private String titulo;
        // ------------Getters------------
        public String getIsbn() {
            return isbn;
        }
        public int getAnyo() {
            return anyo;
        }
        public String getTitulo() {
            return titulo;
        }
        // -------------Setters------------
        public void setIsbn(String isbn) {
            this.isbn = isbn;
        }

        public void setAnyo(int anyo) {
            this.anyo = anyo;
        }

        public void setTitulo(String titulo) {
            this.titulo = titulo;
        }


        @Override
        public String toString (){
            return isbn + " - " + titulo + " - " + anyo;
        }

        // Constructores
        public Publicacion (String isbn, String titulo, int anyo){
            this.isbn=isbn;
            this.titulo=titulo;
            this.anyo=anyo;
        }
    }
    public interface Prestable {

        // Métodos
        void presta();
        boolean estaPrestado();
        void devuelve();

    }
    public static class Libro extends Publicacion implements Prestable {
        // Atributos
        private boolean prestado;
        // Getter
        public boolean getPrestado() {
            return prestado;
        }
        // Setter
        public void setPrestado(boolean prestado) {
            this.prestado = prestado;
        }
        // Métodos

        @Override
        public void presta() {
            setPrestado(true);
        }

        @Override
        public void devuelve() {
            setPrestado(false);
        }

        @Override
        public boolean estaPrestado() {
            return getPrestado();
        }

        @Override
        public String toString(){
            return super.toString() + " - " + (prestado? "Prestado":"Disponible");
        }

        // Constructor
        public Libro (String isbn, String titulo, int anyo){
            super(isbn, titulo, anyo);
            prestado = false;
        }
    }
    public static class Revista extends Publicacion{
        // Atributos
        private int numero;
        // Getter

        public int getNumero() {
            return numero;
        }
        //Setter

        public void setNumero(int numero) {
            this.numero = numero;
        }

        // Constructor
        public Revista (String isbn, String titulo, int anyo, int numero){
            super(isbn, titulo, anyo);
            this.numero=numero;
        }
    }

}
