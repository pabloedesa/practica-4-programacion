package ejercicio1y3;
/**
* Torres Edesa, Pablo
* Practica Evaluable Unidad 6
* Ejercicio 1 totalmente implementado
*/
public class P4Ejercicio1 
{
    public static void main(String[] args) 
    {
		Terminal t1 = new Terminal ("678 11 22 33");
		Terminal t2 = new Terminal ("644 74 64 99");
		Terminal t3 = new Terminal ("622 79 08 24");
		Terminal t4 = new Terminal ("678 12 23 56");
		System.out.println(t1);
		System.out.println(t2);
		t1.llama(t2,320);
		t1.llama(t3, 200);
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t3);
		System.out.println(t4);
    }
}
class Terminal 
{
	// Atributos
	private String numeroTelefono;
	private int tiempoHablado;
	private String informacion;
	// Getters
	public int getTiempoHablado ()
	{
			return tiempoHablado;
	}
	public String getNumeroTelefono ()
	{
			return numeroTelefono;
	}
	public String getInformacion ()
	{
			return informacion;
	}
	// Setters
	public void setTiempoHablado (int tiempoHablado)
	{
			this.tiempoHablado += tiempoHablado;
	}
	public void setNumeroTelefono (String numeroTelefono)
	{
			this.numeroTelefono = numeroTelefono;
	}
	public void setInformacion ()
	{
			informacion  = "N° " + numeroTelefono + " - " + tiempoHablado + "s de conversación";
	}
	// Métodos
	public void llama (Terminal receptor, int duracion)
	{
			setTiempoHablado(duracion);
			receptor.setTiempoHablado(duracion);
			setInformacion();
			receptor.setInformacion();
	}

	@Override
	public String toString()
	{
			return informacion;
	}

	// Constructor
	public Terminal (String numeroTelefono) 
	{
			this.numeroTelefono = numeroTelefono;
			tiempoHablado = 0;
			informacion  = "N° " + numeroTelefono + " - " + tiempoHablado + "s de conversación";
	}
}